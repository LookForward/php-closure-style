<?php

function conj($arr, $el) {
	return array_merge([$el], $arr);
}

function println($str) {
	echo $str . PHP_EOL;
}

$left = function($node) { return $node["left"]; };
$right = function($node) { return $node["right"]; };

function readLineMsg($msg) {
	println($msg);
	echo "> ";
	$line = trim(fgets(STDIN));
	if (!$line) {
		return readLineMsg($msg);
	} else {
		return $line;
	}
}

function readYesNo($msg, $yesFun, $noFun) {
	$line = readLineMsg($msg);
	if ($line == 'да') {
		return $yesFun;
	} elseif ($line == 'нет') {
		return $noFun;
	} else {
		readYesNo($msg, $yesFun, $noFun);
	}
}

function buildNode($newNode, $answerStack) {
	global $left;
	$leaf = $answerStack[0]['answer'];
	$node = $answerStack[0]['node'];
	if (empty($answerStack)) {
		return $newNode;
	} else {
		$node[$leaf == $left ? 'left' : 'right'] = $newNode;
		return buildNode($node, array_slice($answerStack, 1));
	}
}

function addNewAnimal($answerStack) {
	$animal = readLineMsg("Напишите название животного, которое загадали: ");
	$question = readLineMsg("Напишите вопрос: ");
	return buildNode(['text' => $question, 'type' => 'question', 'left' => ['text' => $animal, 'type' => 'animal']], $answerStack);
}

function isAnimal($node) {
	return $node['type'] == "animal";
}

function isQuestion($node) {
	return $node['type'] == "question";
}

function makeQuestion($node) {
	if (isAnimal($node)) {
		return "Это {$node['text']}?";
	} else {
		return $node['text'];
	}
}

function win($answerStack) {
	println("Win");
	return array_pop($answerStack)["node"];
}

function lose($answerStack) {
	println("Lose");
	return addNewAnimal($answerStack);
}

function turn($node, $answerStack = []) {
	global $left, $right;
	if (func_num_args() == 2) {
		$answer = readYesNo(makeQuestion($node), $left, $right);
		$nextNode = $answer($node);
		$updatedStack = conj($answerStack, ["answer" => $answer, "node" => $node]);
		if ($nextNode) {
			return turn($nextNode, $updatedStack);
		} elseif ($answer == $left && isAnimal($node)) {
			return win($updatedStack);
		} else {
			return lose($updatedStack);
		}
	} else {
		if (empty($node)) {
			return addNewAnimal([]);
		} else {
			return turn($node, []);
		}
	}
}

function loadAnimals($filename) {
	return [];
}

function saveAnimals($filename, $node) {
}

function start($node, $f) {
	println(!empty($node) ? "Загадайте животное" : "База пуста");
	$newNode = turn($node);
	saveAnimals($f, $newNode);
	if (readYesNo("Хотите еще сыграть?", true, false)) {
		return start($newNode, $f);
	}
}

function main($f = '') {
	return start(loadAnimals($f), $f);
}

main();